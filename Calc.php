<?php

	header("Content-type: text/xml");
	$operation = $_GET['operation']; 
	$param1 = $_GET['param1']; 
	$param2 = $_GET['param2'];

	if($operation=="add"){
		$result = $param1+$param2;
	} else if($operation=="subtract"){ 
		$result = $param1-$param2;
	}else{
		echo "<?xml version=\"1.0\"?><error><usageInfo>You can use operation add
		and subtract only. This service requires two parameters named param1 and param2. You may use url Calc.php?operation=add&amp;param1=10&amp;param2=20</usageInfo></error>";
		exit(); 
	}

	$doc = new DomDocument('1.0');

	$root = $doc->createElement('calculator');

	$root = $doc->appendChild($root);

	$operationNode = $doc->createElement('operation');

	if ($operation == "add") {
		$operationNode->setAttribute('name','add'); 
	} else if ($operation == "subtract") {
		$operationNode->setAttribute('name','subtract');
	}
	
	$root->appendChild($operationNode);

	$resultNode = $doc->createElement('result');

	$resultValue = $doc->createTextNode($result);

	$resultNode->appendChild($resultValue);

	$operationNode->appendChild($resultNode);

	$xml_string = $doc->saveXML();
	
	echo $xml_string;

?>